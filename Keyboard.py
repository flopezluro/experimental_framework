from Utils.Debug import printOut

class Keyboard():
    """
    trivial wrapper around the panda messenger focused on EVENT handling, not only keyboard handling
    A message can be a simple key stroke, a combination of keys or a word
    One message or event can be linked to ONLY ONE callback.
    """

    def __init__(self, msn):
        '''
        Constructor for the keyboard class
        World is an instance of DirectObject so it can use accept to register to messages.
        :param msn: World
        :return: None
        '''
        self.msn = msn

        self.data = {}
        # one key or event can trigger many callbacks.
        # they are stored as tuples (with all required info), within this dictionary.
        self.keysBindings = {}

        self._keysOrder = []

    def dispatcher(self, *arguments):
        if len(arguments)>0:
            key_pressed = arguments[0]
            try:
                for callbackInfo in self.keysBindings[key_pressed]:
                    # callbackInfo = (registrant, callback, arguments, comment, once, called, display)
                    try:
                        # treat messenger differently (only one message, not as a list)
                        if messenger.send == callbackInfo[1]:
                            callbackInfo[1](callbackInfo[2][0])
                        else:
                            # here just past the list to the function
                            callbackInfo[1](callbackInfo[2])
                    except Exception,e:
                        print e
                        printOut("error calling callback: %s" % str(arguments), 0)
            except IndexError as indexError:
                printOut("error in Keyboard:dispatcher, processing a key event but no information about this"
                         "key is registered, this is bug!")
                print IndexError


    def registerKey(self, key, registrant, callback,
                    comment="", once=False,  arguments=[], display=False):
        """ registers a key with the messenger, to call a callback with
        arguments when something happens, it also remembers the order
        in which keys has been added, and if display is True
        it will show the key when the help is shown (using key ctrl-h)"""
        called = False
        # tuple to save for this key
        callbackInfo = (registrant, callback, arguments, comment, once, called, display)
        registered = self.keysBindings.get(key, None)
        if registered:
            self.keysBindings[key].append(callbackInfo)
        else:
            self.keysBindings[key] = [callbackInfo]
            self._keysOrder.append(key)
            # always send the key/event as the first extraArgs, so that in the dispatcher we know
            # what to look for.
            if once:
                self.msn.acceptOnce(key, self.dispatcher, [key] + arguments )
            else:
                self.msn.accept(key, self.dispatcher, [key] + arguments)
        printOut("Registering key: %s to callback %s for %s" %
                 (key, str(callback), registrant) + "with args:" + str([key] + arguments), 2)
        return True


    def getKeys(self):
        """Returns registered keys"""
        # only return those that are displayable!
        # callbackInfo = (registrant, callback, arguments, comment, once, called, display)
        showKeys = [ k for k in self._keysOrder[:] if
                     any([display[6] for display in self.keysBindings[k]]) ]
        return showKeys

    def getTextKey(self, key):
        """for a given key, returns the comment that explains what it does"""
        printOut("getTextKey with %s" % key, 3)
        # callbackInfo = (registrant, callback, arguments, comment, once, called, display)
        output = "Key %s: " % key
        for binding in self.keysBindings[key]:
            output += binding[3] + '; '
        output += '\n'
        return output

    def unregKey(self, key, caller):
        if key not in self.keysBindings.keys():
            # printOut("WARNING: trying to remove key that was not registered: %s" % key, 0)
            return False

        if caller is None or len(self.keysBindings[key])==1:
            self.msn.ignore(key)
            self.keysBindings.pop(key)
            self._keysOrder.remove(key)
            return True

        # keysBindings has more than one keybinding, so we need to find which
        # one we want to remove from the list, but not "ignore" the key...
        # callbackInfo = (registrant, callback, arguments, comment, once, called, display)
        try:
            self.keysBindings[key] = [binding for binding in self.keysBindings[key] if binding[0] != caller]
            if len(self.keysBindings[key]) == 0:
                self.msn.ignore(key)
                self.keysBindings.pop(key)
                self._keysOrder.remove(key)
            return True
        except Exception, e:
            printOut("error in Keyboard.unregKey: %s" % str(e), 1)
            return False

    def clearKeys(self):
        """Removes ALL keys and callbacks"""
        [self.unregKey(k, None) for k in self.keysBindings.keys()]


