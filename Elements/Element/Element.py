import os.path
from collections import OrderedDict

from direct.showbase import DirectObject
from panda3d.core import *

import external.yaml as yaml
from Utils.Debug import printOut
from Utils.Logger import Logger
from Utils.Utils import *


class Element(object):
    """
    The simulation is based on a Finite State Machine of
    States/Elements, but with the exception that MORE than one
    element can be active at a particular time...I know, sounds
    horrible, but it allows for reuse of small functionality.
    Each element represents some stimuli/functionality in the experiment,
    for example, one image, a text message, a timer, a game!, a pilot...
    It is a very flat hierarchy, and simplistic design.
    """
    def __init__(self, **kwargs):
        '''
        Element constructor. When the element is constructed is passed the YAML
        configuration that is defined in the experiment file. Those kwargs will be
        added as attributes inside the "config" attribute.
        It also saves some useful attributes to give access to the World, and also
        to the different scene parts (2d, 3d)

        :param kwargs: dict
        :return: None
        '''
        # this changes when the state is activated or deactivated.
        self.active = False

        # baseTime for the experiment, from the World object.
        self.baseTime = kwargs['world'].baseTime

        # some colours constants to share among all elements
        self.colours = getColors()

        # don't even remember why I needed this.
        # TODO: REMOVE AND SEE WHAT BREAKS
        self.mouseListener = DirectObject.DirectObject()

        dictionary = {}
        # configuration can be in the experiment file or in a special file
        # just for this element.
        if 'file_config' in kwargs:
            try:
                dictionary = yaml.load(open(kwargs['file_config']))
                kwargs['world'].watchFile(kwargs['file_config'])
            except Exception, e:
                print e
                printOut("Fatal error loading config file " + kwargs['file_config'], 0)
                kwargs['world'].quit()

        # ALL atributes are created under "self.config"
        for k, v in kwargs.items():
            # make every argument from the 'file config' file an attribute, including a reference
            # to world through self.config.world
            if k in dictionary:
                printOut("Warning, same key found in config file and in experiment file",0)
                if 'dict' in str(type(v)):
                    printOut("I will try to join both dictionaries, but if more keys are found they"
                             "will be overriden",0)
                    for k2 in v:
                        dictionary[k][k2] = v[k2]
                if 'list' in str(type(v)):
                    for e in v:
                        dictionary[k].append(e)
            else:
                dictionary[k] = v

        # make everything in the config file a simple object.
        self.config = objFromDict(dictionary)
        printOut("CONFIG LOADED FOR %s" % self.config.name, 4)

        # keep a reference to the original YAML config
        # TODO: Verify that this is not used.
        # self.yaml_config = dictionary


        # try to set some default values, each Element subclass can define
        # a set of default values that if not present in the config file are
        # set here. To know the names of these values, check the constructor
        # of each Element, the attribute "defaults"
        if getattr(self,"defaults",False):
            for k,v in self.defaults.items():
                if not hasattr(self.config, k):
                    printOut("Using default value for "+self.config.name+": "+k+" : "+str(v),0)
                    setattr(self.config, k, v)


        # add the basic 2D and 3D nodepaths in the scenegraph for this element
        # this is needed for rendering stuff on screen, so not for all elements, but I do not
        # want to go down the road a class hierarchy for now.
        self.sceneNP = NodePath(self.config.name)
        self.sceneNP.hide()
        self.hudNP = NodePath(self.config.name+'_hud')
        # set a special BIN (rendering order) for the hudNP
        self.hudNP.setBin('fixed',5)
        self.hudNP.hide()
        # these are basic nodepaths to attach the specific stuff of this
        # element. (text,graphics,anything else)
        self.config.world.addNode(self.sceneNP, place='3d')
        self.config.world.addNode(self.hudNP, place='HUD')

        # we will keep a list of they 'keys' or 'events' defined by
        # this Element so that when we exit the state we unregister them.
        self.registeredKeys = []

    def log(self, message):
        self.config.world.logEvent(message)

    def setTimeOut(self,time):
        # after 'time' a message 'timeout' will be send to the message manager,
        # and if anyone registered to listen for 'timeout' messages they will
        # react
        taskMgr.doMethodLater(time, self.config.world.advanceFSM, 'timeout', extraArgs=[])


#===============================================================================
    def registerKeys(self):
        """Try to setup some keys to callbacks using the YAML file
        specific for this element, or the keys dictionary key if exists
        in the Element description"""

        # Keys defined in the EXPERIMENT FILE, when the Element is created
        config_keys = getattr(self.config, "keys", [])
        keys = config_keys

        # reference to keyboard object to register keys
        keyboard = self.config.world.keyboard

        at_least_one_key_registered = False;

        printOut("Registering keys for the current element", 4)
        for k in keys:
            # Here a key can be really anything, including a dot separated name,
            # where the name belongs to an existing element in the simulation, or
            # a global name in the simulation, such as:
            # "anElement.method" and "messenger.send"


            # take FIRST token of the name (an obect)
            tokens = k.callback.split('.')
            elements = self.config.world.elements
            callback = None
            # we treat specially the case the user writes "send" or "messenger.send" to clearly indicate
            # that this method already exists and it is simply sending a message using the messenger.
            if k.callback == 'messenger.send' or k.callback == 'send':
                callback = messenger.send
            elif len(tokens) == 1:
                callback = getattr(self, tokens[0], None)
            elif len(tokens) == 2:
                if tokens[0] in elements.keys():
                    callback = getattr(elements[tokens[0]], tokens[1], None)

            try:
                if callback is None:
                    printOut('Error!, callback "%s" for event "%s" missing when setting up "%s"' % (tokens[0],k, self.config.name, 0))
                    printOut('Ignoring key binding', 0)
                    continue
            except Exception, e:
                print e

            comment = getattr(k, 'comment', '')
            key = getattr(k, 'key', None)
            # ensure we treat it as a string
            key = str(key)
            once = getattr(k, 'once', False)
            args = getattr(k, 'tuple_args', [])

            # force args to be a list...
            if not isinstance(args, list):
                args = [args]

            if key is None or callback is None:
                printOut('Error!, key or callback missing in when '
                         'setting up %s, check your config file' % self.config.name, 0)
                printOut('Ignoring key binding', 0)
                continue

            # something breaks.
            if len(key.split(',')) > 1:
                printOut("Only one key can be assigned, ignoring key",0)
                continue

            # this could return false in case the key has already been registered
            # in that case the method registerKey from the kbd will warn about that.
            if keyboard.registerKey(key, self.config.name, callback, comment, once, args):
                    self.registeredKeys.append(key)
                    at_least_one_key_registered = True
        else:
            if not at_least_one_key_registered:
                printOut("No keys added by element %s" % self.config.name, 1)

    def unregisterKeys(self):
        keyboard = self.config.world.keyboard
        printOut("De-registering ALL keys for the current element", 4)
        for k in self.registeredKeys:
            if keyboard.unregKey(k,self.config.name):
                printOut("Unregistered key from %s: %s" % (self.config.name, k), 2)
            else:
                pass
                # printOut("Unable to unregister key from %s: %s" % (self.config.name, k), 0)
        self.registeredKeys = []

    def needsToSaveData(self):
        return False

    def saveUserData(self):
        return

    def hideElement(self):
        #print "HIDE ELEMENT IN Element.py " + self.config.name
        self.hudNP.hide()
        self.sceneNP.hide()

    def showElement(self):
        self.hudNP.show()
        self.sceneNP.show()

#    def sendMessage(self, message):
#        # messenger.send(message,[message])
#        messenger.send(message)

    def enterState(self):
        """
        This method will be executed when the Finite State Machine
        enters into this state
        """
        # WE CREATE THE logFile HERE BECAUSE AN ELEMENT CAN BE EXECUTED MORE THAN ONCE.
        # THIS IMPLIES THAT THIS AUTOMATIC LOGFILE CANNOT BE USED IN THE CONSTRUCTOR OF THE
        # ELEMENT.
        # create a logfile using the name of the Element, and the logFilePath if defined,
        # otherwise create the logfile in the default 'run' directory
        # If the logFile with the exact same name exists, it means that the same
        # element has been already executed, and it is being repeated.
        # Add a counter to the last part of the name to account for this.

        if getattr(self.config,'log',False):
            logFilePath = getattr(self.config, "logFilePath", 'run')
            fileCounter = 1
            fname = '%s/%s_%s_0.txt' % (logFilePath,self.config.name,self.config.world.participantId)
            # repeat until a filename is found
            while os.path.isfile(fname):
                fname = fname[:fname.rfind('_')+1]+str(fileCounter)+'.txt'
                fileCounter+=1
        else:
            # nolog is special name to disable logging
            fname = 'nolog'
        self.logFile = Logger(self.baseTime, fname, 'w')
        printOut("Entering state %s" % self.config.name, 1)

        # log all global variables before anything into the log file.
        self.logFile.logEvent("Global state:")
        for name,value in self.config.world.globals.items():
            self.logFile.logEvent("%s: %s" % (name, value))

        self.showElement()

        # is there a timeout set for this state ?
        # a timeout automatically sends an event after the time, with the name of the
        # element prepended with the word "timeout_"
        t = getattr(self.config, 'timeout', None)
        if t is not None:
            try:
                t = float(t)
                if t:
                    taskMgr.doMethodLater(t, messenger.send, 'timeout_'+self.config.name, extraArgs=['timeout_'+self.config.name])
            except:
                printOut("error converting timeout value %s in %s " % (str(t), self.config.name), 0)
                self.config.world.quit()

        # finally, there is a refsto attribute in the config file, which allows to provide a reference from
        # one Element to another, provided that the target reference exists.
        refs = getattr(self.config, "refsto", [])
        el = None
        for ref in refs:
            if ref in self.config.world.elements:
                el = self.config.world.elements[ref]
                setattr(self,ref, el)
            else:
                printOut("ERROR: Element %s trying to set a reference to non-existent element %s!" %
                         (self.config.name, ref ),0)

        # set clear color if it has been explicitly stated in the config
        c = getattr(self.config,'color_background',None)
        if c:
            base.setBackgroundColor(Vec4(*c))
        else:
            base.setBackgroundColor(Vec4(* self.config.world.camera.clear_color))
        # lastly, register keys.
        self.registerKeys()
        self.active = True

    def removeElement(self):
        self.hideElement()
        self.sceneNP.detachNode()
        self.hudNP.detachNode()

    def exitState(self):
        """
        This method will be executed when the Finite State Machine
        exits from this state
        """
        self.hideElement()
        taskMgr.remove('timeout_'+self.config.name)
        self.unregisterKeys()
        printOut("Leaving state %s" % self.config.name,2)
#        self.config.world.createTextKeys()
        self.active = False

        if self.logFile:
            self.logFile.stopLog()

    def isActive(self):
        return self.active

