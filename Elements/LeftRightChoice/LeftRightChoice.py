# panda imports
from direct.gui.DirectGui import *
from direct.gui.OnscreenImage import OnscreenImage

from Elements.Element.Element import *
# os module to list filenames in a directory
import os,itertools, sys
import random
from Utils.Debug import printOut
from Utils.Logger import Logger

class LeftRightChoice(Element):
    """
    Class to drive a LEFT vs RIGHT 2AFC comparison.
    The LEFT and RIGHT Elements support changing the stimuli, given a name, with
    a method "set_stimuli(name)" where name can be an image, or a video, ...

    @TODO: Implement all these alternatives.
    The element allows to select from different forms of pairs.
    Configuration properties:
    one of:
        [pairs pairs_prefix | names names_prefix | directory directory_extensions]
    all:
        leftElement rightElement choice_text
    defaults:
        random: True
        answers: "run/left_right_choice_participantId.txt"

    - 'pairs': then every pair in this list is presented.                                   NOT DONE.
    - 'pairs_prefix' and 'names_prefix' are just strings to prepend for the names           DONE.
    - 'names', then pairs are constructed out of those names, all versus all.               DONE.
    - 'directory': is present, the pairs are built from files matching a certain criteria   NOT DONE.
    -    directory_extensions defines which file extensions to consider.                    NOT DONE.

    - 'random': pairs are presented in random order every time.                             NOT DONE.
    - leftElement and rightElement refer to the left and right stimuli.                     MANDATORY
      They should support calling set_stimuli(name), and start_stimuli() and stop_stimuli()
    - choice_text defines the text to display to the participant.
    - answers:
    """

    def __init__(self, **kwargs):
        """
        Class constructor
        """
        # call parent constructor
        super(LeftRightChoice,self).__init__(**kwargs)

        # used a counter to know how many times the same element has been re-entered.
        # TODO: This should be in the general Element class
        self.reenter = 0

#        # label text
        choice_text = getattr(self.config, 'choice_text', "choice_text")
        label = OnscreenText( text = choice_text, pos = (0,-0.9,0), scale = .08,
                              fg= [1.0,1.0,1.0,1.0], align=TextNode.ACenter, mayChange=0 )

        label.reparentTo(self.hudNP)
#

    def setupPairsFromNames(self, list_names, prefix):
        """make unique pairs without repetition from list_names"""

        seed = random.randrange(sys.maxsize)
        rng = random.Random(seed)

        temp_pairs = [ (x,y) for (x,y) in itertools.product(list_names, list_names) if x!=y ]
        final_pairs = []
        # randomize, and random what goes on the left and what on the right
        while len(temp_pairs)>1:
            x,y = rng.choice(temp_pairs)
            final_pairs.append((x,y))
            temp_pairs.remove((x,y))
            temp_pairs.remove((y,x))
        return [ (prefix+x, prefix+y) for (x,y) in final_pairs ]

    def setupListPairs(self):
        pass

    def nextComparison(self, *args):
        if len(self.comparisonPairs)>0:
            self.curr_pair = self.comparisonPairs.pop(0)
            self.leftElement.set_stimuli(self.curr_pair[0])
            self.leftElement.start_stimuli()
            self.leftElement.showElement()
            self.rightElement.set_stimuli(self.curr_pair[1])
            self.rightElement.start_stimuli()
            self.rightElement.showElement()
        else:
            messenger.send('comparison_finished')

    def choiceMade(self,args):
        """This method should be called when one choice is made."""

        # remove everthing from the path, and the extension (one dot only)
        left = os.path.basename(self.curr_pair[0])[:-4]
        right = os.path.basename(self.curr_pair[1])[:-4]
        self.logResults.logEvent(left + ':' + right + ":" + str(args))
        self.nextComparison(args)

    def enterState(self):
        Element.enterState(self)

        self.reenter += 1

        # initialise comparison targets when we enter the Element, not when is constructed.
        # This can be useful in case the same element has to be entered more than once.
        if getattr(self.config, 'names', None):
            names_prefix = getattr(self.config, 'names_prefix', '')
            self.comparisonPairs = self.setupPairsFromNames(self.config.names, names_prefix)
            pass
        else:
            # TODO: add other methods of comparison pairs
            self.comparisonPairs = []
            pass

        # get references to left and right elements.
        # we do this here, because the elements might not exist at the time of object creation.
        try:
            self.leftElement = self.config.world.elements[self.config.leftElement]
            self.rightElement = self.config.world.elements[self.config.rightElement]
        except KeyError, ke:
            print ke
            print "left and right elements must be valid elements in the YAML file"

        # file output to store the results
        outputDir = getattr(self.config,'output_answers','run')
        logName = os.path.join(outputDir, self.config.name+"_"+self.config.world.participantId+".log")
        if self.reenter > 1:
            logName = logName.replace('.log', '_'+str(self.reenter)+'.log')
        self.logResults = Logger(self.baseTime, logName,mode='w')

        self.nextComparison()

    def exitState(self):
        # we could unload the images here...
        Element.exitState(self)
        self.logResults.stopLog()


