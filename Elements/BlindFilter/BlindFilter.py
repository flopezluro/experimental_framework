__author__ = 'Francholi'
__author__ = 'Francholi'

# panda imports
from Elements.Element.Element import *
from panda3d.core import Shader
from direct.filter.FilterManager import FilterManager
from direct.filter.CommonFilters import CommonFilters

class BlindFilter(Element):
    """

    """
    def __init__(self, **kwargs):
        """
        See the Element class to find out what attributes are available
        from scratch
        """
        super(BlindFilter, self).__init__(**kwargs)
        self.shaderPass = Shader.load(Shader.SL_GLSL, vertex="Elements\BlindFilter\passThrough.glsl",fragment="Elements\BlindFilter\colorBlindFrag.glsl")
        self.filterActive = False
        self.hideElement()

    def toggleFilter(self, arg):
        if (not self.filterActive):
            self.manager2d = FilterManager( base.win, base.cam2d )
            self.manager = FilterManager( base.win, base.cam )

            self.fullScreenBuffer2D = Texture()
            self.fullScreenBuffer = Texture()

            self.quad2D = self.manager2d.renderSceneInto(colortex=self.fullScreenBuffer2D)
            self.quad2D.setShader(self.shaderPass)
            self.quad2D.setShaderInput("tex",self.fullScreenBuffer2D)

            self.quad = self.manager.renderSceneInto(colortex=self.fullScreenBuffer)
            self.quad.setShader(self.shaderPass)
            self.quad.setShaderInput("tex",self.fullScreenBuffer)
            self.filterActive = True
        else:
            self.manager2d.cleanup()
            self.manager.cleanup()
            self.filterActive = False

    def enterState(self):
        # super class enterState
        Element.enterState(self)
        #self.manager = CommonFilters( base.win, base.cam )
        #self.manager.setInverted()
        #self.manager2d.setInverted()


    def exitState(self):
        # super class exitState
        # self.manager.cleanup()
        Element.exitState(self)


