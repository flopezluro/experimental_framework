#version 430

uniform sampler2D tex;

in vec2 texcoord;
out vec4 fragColor;

void main() {
    mat3 rgb2lms = mat3(17.8824, 3.4557, 0.02996, 43.5161, 27.1554, 0.18431, 4.1193, 3.8671, 1.4670);
    vec4 color = texture(tex, texcoord);
    vec3 lms = rgb2lms * color.rgb;
    lms.r = lms.g*2.02344 + lms.b*-2.52581;
    fragColor = vec4(inverse(rgb2lms) * lms, 1.0);
}